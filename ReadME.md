# Log4Fuck

Honestly, log4j, slf4j, whatever. Nobody needs that stuff. They just bring handy exploits.
This logging pretty much just replaces the `System.out` stream and appends the current time, a log level and the current thread to it.

## Usage

Simple call

```kotlin
Log.log(LogLevels.VERBOSE) { "Hello world!" }
```

## Get it

### Maven

```xml
<dependency>
    <groupId>de.bixilon</groupId>
    <artifactId>log4fuck</artifactId>
    <version>1.1</version>
</dependency>
```

### Gradle

```groovy
implementation 'de.bixilon:log4fuck:1.1'
```

```kotlin
implementation("de.bixilon:log4fuck:1.1")
```
