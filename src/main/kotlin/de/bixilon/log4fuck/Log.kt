/*
 * log4fuck
 * Copyright (c) 2024 Moritz Zwerger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.bixilon.log4fuck

import de.bixilon.kutil.ansi.ANSI
import de.bixilon.kutil.exception.ExceptionUtil.catchAll
import de.bixilon.kutil.exception.ExceptionUtil.toStackTrace
import de.bixilon.kutil.shutdown.ShutdownManager
import de.bixilon.kutil.time.TimeUtil.millis
import de.bixilon.log4fuck.tag.LogTag
import java.io.PrintStream
import java.text.SimpleDateFormat
import java.util.concurrent.LinkedBlockingQueue
import kotlin.concurrent.thread

object Log {
    private val TIME_FORMAT = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    private val queue = LinkedBlockingQueue<QueuedMessage>()


    init {
        init()
    }

    fun init() {
        LogProperties // init, gets original system streams
        thread(name = "Log") {
            while (true) {
                try {
                    queue.take().print()
                } catch (error: Throwable) {
                    error.printStackTrace(LogProperties.SYSTEM_ERR)
                }
            }
        }
        ShutdownManager.addHook { LogProperties.ASYNC = false; catchAll { await() } }
        System.setOut(LogPrintStream(LogLevels.INFO))
        System.setErr(LogPrintStream(LogLevels.WARN))
    }

    private fun LogLevels.getStream(): PrintStream {
        if (LogProperties.USE_STD_ERR && this <= LogLevels.WARN) return LogProperties.SYSTEM_ERR // you can not synchronize two different streams properly; it messes up the terminal...
        return LogProperties.SYSTEM_OUT
    }

    private val QueuedMessage.prefix: String
        get() {
            val prefix = StringBuilder()

            if (LogProperties.ANSI_COLORS) {
                prefix.append(ANSI.RESET).append(level.formatting)
            }

            prefix.append('[').append(TIME_FORMAT.format(time)).append(']').append(' ')
            prefix.append('[').append(thread.name).append(']').append(' ')
            prefix.append('[').append(level).append(']').append(' ')
            tag?.let { prefix.append('[').append(it.prefix).append(']').append(' ') }

            return prefix.toString()
        }

    private fun QueuedMessage.print() {
        val prefix = prefix
        val stream = level.getStream()

        synchronized(stream) {
            for (line in message.lineSequence()) {
                if (line.isBlank()) continue

                stream.print(prefix)
                stream.print(line)
                if (LogProperties.ANSI_COLORS) {
                    stream.print(ANSI.RESET)
                }
                stream.print(LogProperties.SEPARATOR)
            }
            stream.flush()
        }
    }

    private fun QueuedMessage.enqueue() {
        if (message.isBlank()) return
        if (LogProperties.ASYNC) {
            queue += this
        } else {
            this.print()
        }
    }

    fun enqueue(tag: LogTag?, level: LogLevels, message: Any?) {
        QueuedMessage(Thread.currentThread(), millis(), tag, level, message.format()).enqueue()
    }

    fun LogLevels.log(): Boolean {
        if (this > LogProperties.LEVEL) {
            return false
        }
        return true
    }

    fun LogLevels.log(tag: LogTag): Boolean {
        return tag.canLog(this)
    }

    fun log(level: LogLevels, message: Any?) {
        if (!level.log()) return
        enqueue(null, level, message)
    }

    inline fun log(level: LogLevels, message: () -> Any?) {
        if (!level.log()) return
        enqueue(null, level, message.invoke())
    }

    inline fun fatal(message: () -> Any?) = log(LogLevels.FATAL, message)
    inline fun warn(message: () -> Any?) = log(LogLevels.WARN, message)
    inline fun info(message: () -> Any?) = log(LogLevels.INFO, message)
    inline fun verbose(message: () -> Any?) = log(LogLevels.VERBOSE, message)
    inline fun debug(message: () -> Any?) = log(LogLevels.DEBUG, message)

    fun log(level: LogLevels, tag: LogTag, message: Any?) {
        if (!level.log(tag)) return
        enqueue(tag, level, message)
    }

    inline fun fatal(tag: LogTag, message: () -> Any?) = log(LogLevels.FATAL, tag, message)
    inline fun warn(tag: LogTag, message: () -> Any?) = log(LogLevels.WARN, tag, message)
    inline fun info(tag: LogTag, message: () -> Any?) = log(LogLevels.INFO, tag, message)
    inline fun verbose(tag: LogTag, message: () -> Any?) = log(LogLevels.VERBOSE, tag, message)
    inline fun debug(tag: LogTag, message: () -> Any?) = log(LogLevels.DEBUG, tag, message)

    inline fun log(level: LogLevels, tag: LogTag, message: () -> Any?) {
        if (!level.log(tag)) return
        enqueue(tag, level, message.invoke())
    }


    fun await() {
        while (this.queue.isNotEmpty()) {
            Thread.sleep(1)
        }
    }

    private fun Any?.format(): String = when {
        this is Throwable -> this.toStackTrace()
        else -> this.toString()
    }
}
