/*
 * log4fuck
 * Copyright (c) 2024 Moritz Zwerger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.bixilon.log4fuck

import de.bixilon.kutil.stream.NullOutputStream
import java.io.PrintStream

class LogPrintStream(
    val level: LogLevels,
) : PrintStream(NullOutputStream()) {

    override fun print(string: String?) {
        if (string == null) {
            return
        }
        if (string.startsWith("SLF4J: ")) {
            return
        }
        if (string.startsWith("ERROR StatusLogger Log4j2")) {
            return
        }
        if (string.startsWith("Unknown element") && string.endsWith("):")) {
            return
        }
        if (Thread.currentThread().name == "JavaFX-Launcher") {
            return
        }
        Log.log(level = this.level, string)
    }

    override fun write(buf: ByteArray, off: Int, len: Int) {
        val target = ByteArray(len - off)
        System.arraycopy(buf, off, target, 0, len)
        print(String(target))
    }
}
