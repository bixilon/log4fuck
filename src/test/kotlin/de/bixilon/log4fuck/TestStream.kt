/*
 * log4fuck
 * Copyright (c) 2024 Moritz Zwerger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.bixilon.log4fuck

import de.bixilon.kutil.stream.NullOutputStream
import java.io.PrintStream

class TestStream : PrintStream(NullOutputStream()) {
    val lines: MutableList<String?> = mutableListOf()
    private val builder = StringBuilder()

    override fun print(string: String?) {
        if (string == null) return
        val lines = string.lines()
        for ((index, line) in lines.withIndex()) {
            builder.append(line)
            if (index < (lines.size - 1)) {
                push()
            }
        }
    }

    override fun println() {
        push()
    }

    private fun push() {
        if (builder.isEmpty()) return
        lines += builder.toString()
        builder.clear()
    }

    fun assert(vararg messages: String) {
        push()
        for ((index, expected) in messages.withIndex()) {
            val actual = this.lines.getOrNull(index)?.split("] ", limit = 2)?.get(1) // remove date
            if (actual != expected) {
                throw AssertionError("Expected: $expected, but got $actual (index=$index)")
            }
        }
        if (messages.size != this.lines.size) throw IllegalArgumentException("Size does not match: expected=${messages.size}, but got ${this.lines.size}")
    }
}
