/*
 * log4fuck
 * Copyright (c) 2024 Moritz Zwerger
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package de.bixilon.log4fuck

import de.bixilon.kutil.reflection.ReflectionUtil.forceSet
import de.bixilon.log4fuck.tag.LogTag
import kotlin.test.Test

class LogTest {

    private fun create(): Pair<TestStream, TestStream> {
        LogProperties.ASYNC = false
        LogProperties.ANSI_COLORS = false
        LogProperties.USE_STD_ERR = true
        val out = TestStream()
        val err = TestStream()
        LogProperties::SYSTEM_OUT.forceSet(out)
        LogProperties::SYSTEM_ERR.forceSet(err)
        return Pair(out, err)
    }

    @Test
    fun `simple message`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.INFO
        Log.log(LogLevels.INFO) { "Hehe" }
        err.assert()
        out.assert("[Test worker] [INFO] Hehe")
    }

    @Test
    fun `more verbose log level set`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.VERBOSE
        Log.log(LogLevels.INFO) { "Hehe" }
        err.assert()
        out.assert("[Test worker] [INFO] Hehe")
    }

    @Test
    fun `less verbose log level set`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.WARN
        Log.log(LogLevels.INFO) { "Hehe" }
        err.assert()
        out.assert()
    }

    @Test
    fun `warn in stderr`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.WARN
        Log.log(LogLevels.WARN) { "Hehe" }
        err.assert("[Test worker] [WARN] Hehe")
        out.assert()
    }

    @Test
    fun `fatal in stderr`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.WARN
        Log.log(LogLevels.FATAL) { "Hehe" }
        err.assert("[Test worker] [FATAL] Hehe")
        out.assert()
    }

    @Test
    fun `always log tag`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.FATAL
        Log.log(LogLevels.INFO, SimpleLogTag(true)) { "tagged message" }
        err.assert()
        out.assert("[Test worker] [INFO] [ABC] tagged message")
    }

    @Test
    fun `never log tag`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.DEBUG
        Log.log(LogLevels.INFO, SimpleLogTag(false)) { "tagged message" }
        err.assert()
        out.assert()
    }

    @Test
    fun `simple log print stream`() {
        val stream = LogPrintStream(LogLevels.INFO)
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.INFO
        stream.println("Test")
        err.assert()
        out.assert("[Test worker] [INFO] Test")
    }

    @Test
    fun `ignore slf4j warning`() {
        val stream = LogPrintStream(LogLevels.INFO)
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.INFO
        stream.println("SLF4J: no no no")
        err.assert()
        out.assert()
    }

    @Test
    fun `multiple messages`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.INFO
        Log.log(LogLevels.INFO) { "message 1" }
        Log.log(LogLevels.INFO) { "message 2" }
        err.assert()
        out.assert("[Test worker] [INFO] message 1", "[Test worker] [INFO] message 2")
    }

    @Test
    fun `multi line message`() {
        val (out, err) = create()
        LogProperties.LEVEL = LogLevels.INFO
        Log.log(LogLevels.INFO) { "message 1\nmessage 2" }
        err.assert()
        out.assert("[Test worker] [INFO] message 1", "[Test worker] [INFO] message 2")
    }

    private class SimpleLogTag(
        val log: Boolean,
        override val prefix: String = "ABC",
    ) : LogTag {

        override fun canLog(level: LogLevels) = log
    }
}
